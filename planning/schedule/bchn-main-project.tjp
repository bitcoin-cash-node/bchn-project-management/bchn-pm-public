# Bitcoin Cash Node
# main project plan
#
# Copyright 2020 freetrader <freetrader@tuta.io>
#
# Adapted from Fedora-20 project plan example
# Copyright 2011 John Poelstra <poelstra@fedoraproject.org>
# Copyright 2011 Robyn Bergeron <rbergero@fedoraproject.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

# do not change this
macro project_name_long   [Bitcoin Cash Node]
macro project_name_short  [BCHN]

# do change version when you update the plan!
# semantic versioning:
# - change patch version (last digit) for cosmetic fixups
# - change minor version for minor updates do not introduce new commitments
# - change major version for updates that introduce new commitments into
#   the plan or shift existing ones in a major way.
macro bchn_project_plan_version  [0.0.1]

macro start_date    [2020-05-03]
macro start_work    [2020-05-11]
macro end_date      [2020-12-31]

project bchn "${project_name_short}" "${bchn_project_plan_version}" ${start_date} - ${end_date} {
  outputdir "html"
  timeformat "%Y-%b-%d"

  # Based on Europe/GMT
  timezone "Etc/GMT"

  # Use US format for numbers
  numberformat "-" "" "," "." 1
  # Number format for Bitcoin Cash. Still experimenting.
  currencyformat "(" ")" "," "." 8
  # Pick a day during the project that will be reported as 'today' in
  # the project reports. If not specified, the current day will be
  # used, but this will likely be outside of the project range, so it
  # can't be seen in the reports.
  now 2020-05-10-00:00
  # The currency for all money values is Bitcoin Cash (BCH).
  currency "BCH"

  # Limit working days
  workinghours sat,sun off

  # Setup scenarios
  scenario plan "Original Plan" {
    scenario actual "Actual"
  }
}

# ================  Guideline to BCHN Scheduling ====================
# This section also serves as a "style guide" for the source file too

/*
1) EVERY entry should be in the following line order and indented consistently with the rest of the file.  This makes the source file consistent and easier to read.
   a) task name and description
   b) dependency (if applicable)
   c) length (not "duration") (if applicable)
   d) flags (if applicable)

2) Most tasks are scheduled with 'length' instead of 'duration' to get a sense of "work days" required--using 'length' one week = 5 days
--an eternal debate could be held to discuss whether or not schedule calculations
should include weekends because community members work at all times and not within strict work days.

3) Use "length" everywhere and "duration" only when something must take into account a weekend day

4) If a task takes one day or less--schedule with no length--this way it shows up as a milestone

5) Use schedule milestones (flags = key) as anchor points (depends/precedes) for tasks in this schedule file instead of more transient tasks like meetings, compose dates, etc. that may change (or slip) from release to release.  This makes building new release schedules easier and require less maintenance and updating.

6) In ALL but limited cases task beginning and ending should be automatically calculated based on logic in this file.  When using hard coded dates, explicitly call them out with a comment to highlight their existence.  Hard coded dates are particularly troublesome when slipping a schedule or branching the file to create a new release schedule because they must be adjusted and recalculated manually

7) TaskJuggler does not provide an easy way (that I am aware of) to schedule tasks to happen *before* other tasks.  I've created a hack/methodology I call "shadow" tasks.  These are unreported tasks that go backwards a certain period of time and serve as an anchor or starting point for the actual task to be reported.

8) The "milestone" declaration is NOT used in this source file.  It is redundant and unnecessary.  Do not include it.  All tasks without 'length' or 'duration' are automatically considered "milestone" tasks.

*/

# Define flags for filtered reporting
flags backporting
flags bugtriage
flags design
flags devel
flags docs
flags hidden     # used to hide tasks that we do not displayed in BCHN reports
flags infrastructure
flags interface
flags key       # use for report of key tasks/high level overview
flags marketing
flags pm
flags pr
flags proto     # used for drafting new schedules and shows tasks useful for doing this
flags quality
flags releng
flags research  # R&D
flags roadmap   # major milestones
flags specs
flags support
flags testing
flags translation
flags web


# main BCHN project task containing all other tasks
task BCHN "${project_name_short}" {

  start ${start_work}

  task first_day "First Day of Development" {
    flags hidden
  }

  task backport "Backporting" {
    start ${start_work}
    length 6m
    flags key, backporting
  }

  task user_support "User support" {
    start ${start_work}
    length 6m
    flags key, support
  }

  task DAA_evaluation "Evaluate DAA problems and solutions" {
    start ${start_work}
    flags key, research, devel, testing, docs

    task kickoff "Kickoff DAA work" {
      start ${start_work}
      flags specs, docs
    }
    task DAA_eval_report "Perform DAA evaluation to select solution" {
      depends !kickoff
      length 2m   # TODO: should finish early July
      flags devel, testing
    }
    task deliver_eval_report "Deliver DAA evaluation report" {
      depends !DAA_eval_report
      flags docs
    }
    task DAA_code_and_tests "Implement DAA solution code and tests" {
      depends !DAA_eval_report
      length 1m
      flags devel, testing
    }
    task DAA_deliver_code_and_tests "Deliver DAA code and tests" {
      depends !DAA_code_and_tests
      flags devel, testing
    }
  }

  task chained_tx_eval "Investigation of solutions to increase chained tx limit"  {
    flags key, research, specs, devel, testing, docs

    task kickoff "Kickoff tchains work" {
      depends !!DAA_evaluation.DAA_deliver_code_and_tests
    }
    task txchains_benchmarking "Benchmark & find bottlenecks" {
      depends !kickoff
      length 4w
      flags devel, testing, docs
    }
    task txchains_investigate_solutions "Investigate solutions" {
      depends !txchains_benchmarking
      length 4w
      flags devel, testing, docs
    }
    task txchains_report "Compile report" {
      depends !kickoff
      length 10w
      flags docs
    }
    task txchains_publish "Publish txchain report" {
      depends !txchains_report
    }
  }

  task number_ops_in_script "Higher precision numerical script ops" {
    start ${start_work}
    flags key, specs, devel, testing, docs

    task kickoff "Kickoff numerical script ops work" {
      start ${start_work}
    }
    task numops_spec "Complete numerical script ops specification" {
      depends !kickoff
      length 1m   # TODO: should finish early July
      flags specs, docs
    }
    task publish "Publish numops specs" {
      depends !numops_spec
    }
    task numops_code "Implement numops code" {
      depends !numops_spec
      length 2m
      flags devel, testing
    }
    task numops_tests "Implement numops tests" {
      depends !numops_spec
      length 2m
      flags devel, testing
    }
    task deliver_implementation "Publish numops specs and reference implementation" {
      depends !numops_code, !numops_tests
    }
  }

  task website_improvement "Website improvement work" {
    start ${start_work}
    flags key, web

    task self_funding "Self-funding capability (flipstarter or other)" {
      start ${start_work}
      length 2m
    }

    task web_translations "Translation of website into other languages" {
      start ${start_work}
      length 3m
    }

    task web_publication_section "Host project publications on project website" {
      start ${start_work}
      length 10d
      flags key, specs
    }
  }

  task testnets_setup "Setting up testnets" {
    start ${start_work}
    flags key, infrastructure, docs

    task kickoff "Kickoff testnets work" {
      start ${start_work}
    }
    task testnets_planning "Testnets planning" {
      depends !kickoff
      length 1m
      flags infrastructure, docs
    }
    task testnets_setup "Testnets implementation" {
      depends !testnets_planning
      length 5m
      flags infrastructure, docs
    }
  }

  task rolling_checkpoints_specification "Close specification gap for rolling checkpoints" {
    start ${start_work}
    flags key, specs, devel, testing, docs

    task kickoff "Kickoff rolling checkpoints gap work" {
      start ${start_work}
      flags specs, docs
    }
    task specify_checkpoints_gap "Specification" {
      depends !kickoff
      start ${start_work}
      length 3w
      flags specs, docs
    }
    task validate_checkpoints_gap "Validation" {
      depends !specify_checkpoints_gap
      length 7w
      flags testing
    }
    task publish "Publish rolling checkpoints report" {
      depends !validate_checkpoints_gap
      flags specs, docs
    }
  }

  task download_server "Set up a download server" {
    start ${start_work}
    flags key, infrastructure, docs
    length 2d
  }

}

include "bchn-reports.tji"

tagfile "tags"
