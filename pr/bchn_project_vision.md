# BCHN vision

Bitcoin Cash Node is a professional, miner-friendly node that solves practical problems for Bitcoin Cash, peer-to-peer electronic cash for the world.

Our development process is transparent, evidence-driven and responsive to ecosystem needs, resulting in software that is performant, reliable and predictable.

We strongly believe in building an ecosystem that has no single points of failure, and will collaborate to make that a reality.


## Objectives

### Short term

- Establish a professional mining node which listens to feedback and delivers measurable improvements.
- Provide well-researched proposals for consensus rule enhancements regarding DAA, 0-conf and script improvements.
- Lead by example with specification-driven development that enables greater mining node diversity and increases BCH's resistance to capture.

### Medium term

- Demonstrate sustainable client development based on wide ecosystem input and a transparent, voluntary funding model.
- Demonstrate evidence-based, collaborative decision making on consensus changes.
- Maintain and expand network effect where possible.
- Make progress on the scaling work for global adoption through both processing and network performance improvements.
- Cooperate on specification and testing initiatives to diversify the node landscape, stimulate broader participation and protect the BCH network from catastrophic failures.

### Long term

- Raise awareness that BCH must remain permissionless, useful and affordable to use in order to become the best money in the world.
