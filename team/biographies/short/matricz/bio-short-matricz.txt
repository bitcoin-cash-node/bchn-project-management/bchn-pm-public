matricz
Contributor

matricz has a lot of interests, CS/IT being one of them and the main professional focus. He has an inclination for applied cryptography, but work has steered him more towards programming.

He's worked for 15 years on a variety of projects, mostly in small teams where a set of different skills are needed.  Most of his day work is in Java and Javascript, with all of the dev-ops plumbing that is usual in smaller teams.

He says that his C/C++ are a little rusty these days, but that's not a big deal since he's a QA-inclined guy. In fact most of his contributions are in documentation and testing - there's always lots to do besides coding. He has a preference for evidence-based development: benchmarking different solutions against each other helps with removing (inevitable) bias. 

What does he want from Bitcoin? Just a level(-er) playing field for everyone. He's been searching for a long time for a way to contribute toward this goal, and Bitcoin Cash Node is the right opportunity. 

